package com.arisan;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import java.util.ArrayList;
import org.junit.Test;

import junit.framework.Assert;

import java.util.HashSet;
import java.util.Set;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    App arisan = new App();
    private int jumlahPeserta = 15;
    @Test
    public void testUniqueWinner()
    {
        ArrayList<String> winner = new ArrayList<String>();
        for(int i = 0 ; i < jumlahPeserta; i++){
            winner.add(arisan.getWinner());
        }
        Assert.assertEquals(false, isDuplicateWinner(winner));
    }

    public void testUniqueRandomCode(){
        ArrayList<Integer> randomCode = new ArrayList<Integer>();
        for(int i = 0; i < jumlahPeserta; i ++){
            randomCode.add(arisan.getRandomCode());
        }
        Assert.assertEquals(false, isDuplicate(randomCode));
        
    }
    public boolean isDuplicate(ArrayList<Integer> randomCode){
        Set<Integer> set = new HashSet<Integer>(randomCode);

        if(set.size() < randomCode.size()){
            return true;
        }
        return false;
    }

    public boolean isDuplicateWinner(ArrayList<String> randomCode){
        Set<String> set = new HashSet<String>(randomCode);

        if(set.size() < randomCode.size()){
            return true;
        }
        return false;
    }
}
