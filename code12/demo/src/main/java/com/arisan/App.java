package com.arisan;
import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class App 
{
    private static HashMap<String, Integer> pesertaCode = new HashMap<String, Integer>();
    private static String[] pesertaArray = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J","K", "L", "M", "N", "O"};
    private static ArrayList<String> peserta = new ArrayList<String>();
    private static ArrayList<String> jumlahPemenang = new ArrayList<String>();



    public static int getRandomCode(){
        Random rand = new Random();
        for(int i = 0 ; i < peserta.size() ; i ++){
            int randomInt = rand.nextInt(500);
            int counter = 0;
            if(pesertaCode.size() > 0){
                for(int j : pesertaCode.values()){
                    if(j == randomInt){
                        counter +=1;
                    }
                }
            }
            if(counter == 0){
                pesertaCode.put(peserta.get(i), randomInt);
                return randomInt;
            }
            if(counter > 0){
                i = i -1;
            }
        }
        return 0;

    }
    public static String getWinner(){
        String pemenang;
        List keys = new ArrayList(pesertaCode.keySet());
        Collections.shuffle(keys);
        pemenang = keys.get(0).toString();
        jumlahPemenang.add(pemenang);
        peserta.remove(pemenang);
        pesertaCode.remove(pemenang);
        return pemenang;
    }
    public static void main( String[] args )
    {
        //Konversi String Array ke ArrayList 
        peserta.addAll(Arrays.asList(pesertaArray));
  
        int putaran = 1;
        while(true){
            // Pembagian Kode Ke Peserta
            for(int i = 0 ; i < peserta.size() ; i ++){
                pesertaCode.put(peserta.get(i), getRandomCode());
            }
            // Merandom Pemenang
            String pemenang = getWinner();
            System.out.println("Pemenang Arisan Putaran Ke-" +putaran);
            System.out.println("Nama : " + pemenang);
            putaran++;

            if(peserta.size() == 0){
                break;
            }
        }
      
    }
}
